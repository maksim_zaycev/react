import React, { useState } from 'react';
import './App.css';

const App = () => {
	const [query, setQuery] = useState('666');
	const [items, setItems] = useState([
		{
			id: 24572456,
			name: 'Сходить в магазин',
			status: true,
		},
		{
			id: 63845683657,
			name: 'Выбросить мусор',
			status: true,
		},
		{
			id: 3457367,
			name: 'Купить молоко',
			status: true,
		},
		{
			id: 245736823,
			name: 'Помыть пол',
			status: true,
		},
		{
			id: 24573452336823,
			name: 'Помытьwefwer пол',
			status: true,
		},
		{
			id: 24573452,
			name: 'Помытьw4536456efwer пол',
			status: true,
		},
	]);

	const onHandleChange = event => {
		console.log(event.target.value);
		setQuery(event.target.value);
	};

	return (
		<div className="app">
			<div className="add">
				<input className="field" value={query} onChange={onHandleChange} type="text" />
				<button className="button" >Добавить</button>
			</div>
			<div className="list">
				{
					items.map(item => (
						<div key={item.id} className="item">
							{item.name}
						</div>
					))
				}
			</div>
		</div>
	);
}

export default App;
